#!/bin/bash

# Backup the files and directories listed on backupfiles.txt.
# -r - recursively copy all files in directories
# -a - copy all metadata
# -v - verbose (print messages to stdout)
# Copies the files in /home/jcasse/ that match the names in
# backupfiles.txt to /mnt/backup/home/jcasse/.

# Note: If get permission error when trying to chown, then the usb
#       drive is probably not formatted with ext3 or 4.
rsync -arv --no-perms --no-group --no-owner --files-from=backupfiles.txt --exclude='*~' . /run/media/jcasse/F41E-E785/archlive/

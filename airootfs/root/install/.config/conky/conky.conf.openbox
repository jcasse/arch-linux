-- Conky, a system monitor https://github.com/brndnmtthws/conky
--
-- This configuration file is Lua code. You can write code in here, and it will
-- execute when Conky loads. You can use it to generate your own advanced
-- configurations.
--
-- Try this (remove the `--`):
--
--   print("Loading Conky config")
--
-- For more on Lua, see:
-- https://www.lua.org/pil/contents.html

conky.config = {
    alignment = 'top_left',
    background = true,
    border_width = 1,
    cpu_avg_samples = 2,
    default_color = 'gray',
    default_outline_color = 'white',
    default_shade_color = 'white',
    double_buffer = true,
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    extra_newline = false,
    font = 'DejaVu Sans Mono:size=12',
    gap_x = 10,
    gap_y = 10,
    minimum_height = 5,
    minimum_width = 5,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_transparent = true,
    own_window_argb_visual = true,
    own_window_argb_value = 255,
    show_graph_range = false,
    show_graph_scale = false,
    stippled_borders = 0,
    update_interval = 0.33,
    uppercase = false,
    use_spacer = 'none',
    use_xft = true,
}

conky.text = [[
${font hack:bold:size=12}${color #04072b}Info:${font hack:bold:size=12}${color #04072b} ${scroll 32 Conky $conky_version - $sysname $nodename $kernel $machine}
$hr
${font hack:bold:size=12}${color #04072b}Uptime:${font hack:bold:size=12}${color #04072b} $uptime
${font hack:bold:size=12}${color #04072b}Frequency (in MHz):${font hack:bold:size=12}${color #04072b} $freq
${font hack:bold:size=12}${color #04072b}Frequency (in GHz):${font hack:bold:size=12}${color #04072b} $freq_g
${font hack:bold:size=12}${color #04072b}RAM Usage:${font hack:bold:size=12}${color #04072b} $mem/$memmax - $memperc% ${membar 4}
${font hack:bold:size=12}${color #04072b}Swap Usage:${font hack:bold:size=12}${color #04072b} $swap/$swapmax - $swapperc% ${swapbar 4}
${font hack:bold:size=12}${color #04072b}CPU Usage:${font hack:bold:size=12}${color #04072b} $cpu% ${cpubar 4}
${font hack:bold:size=12}${color #04072b}Processes:${font hack:bold:size=12}${color #04072b} $processes  ${font hack:bold:size=12}${color #04072b}Running:${font hack:bold:size=12}${color #04072b} $running_processes
$hr
${font hack:bold:size=12}${color #04072b}File systems:
 / ${font hack:bold:size=12}${color #04072b}${fs_used /}/${fs_size /} ${fs_bar 6 /}
${font hack:bold:size=12}${color #04072b}Networking:
Up:${font hack:bold:size=12}${color #04072b} ${upspeed} ${font hack:bold:size=12}${color #04072b} - Down:${font hack:bold:size=12}${color #04072b} ${downspeed}
$hr
${font hack:bold:size=12}${color #04072b}Name              PID     CPU%   MEM%
${font hack:bold:size=12}${color #04072b} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${font hack:bold:size=12}${color #04072b} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${font hack:bold:size=12}${color #04072b} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${font hack:bold:size=12}${color #04072b} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
$hr
${font hack:bold:size=12}${color #04072b}Key Bindings
${font hack:bold:size=12}${color #04072b} Super + Space            Launcher
${font hack:bold:size=12}${color #04072b} Super + L                Logout
${font hack:bold:size=12}${color #04072b} Super + W + M            Toggle Maximize window
${font hack:bold:size=12}${color #04072b} Super + W + C            Window to Center
${font hack:bold:size=12}${color #04072b} Super + W + H            Window to Left Half
${font hack:bold:size=12}${color #04072b} Super + W + L            Window to Right Half
${font hack:bold:size=12}${color #04072b} Super + Tab              Switch Next Window
${font hack:bold:size=12}${color #04072b} Super + `                Switch Previous Window
]]

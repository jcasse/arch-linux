#
# ~/.bash_profile
#

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Juan Casse
# Start X when in tty1,
# which is the one started after booting the system.
if [[ "$(tty)" = "/dev/tty1" ]]; then
    startx
fi

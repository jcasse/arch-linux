#!/bin/bash
DRIVE=''
DRIVE_PASSPHRASE=''
BOOTLOADER_TARGET='x86_64-efi'
MICROCODE='intel-ucode'
HOSTNAME=''
USER_NAME=''
USER_PASSPHRASE=''
TIMEZONE='America/Los_Angeles'
KEYMAP='us'
VIDEO_DRIVERS='xf86-video-intel xf86-video-nouveau'
SNAPS_DIR='/.snapshots'

phase_1()
{
    local boot_dev="$DRIVE"1
    local swap_dev="$DRIVE"2
    local root_dev="$DRIVE"3
    local crypt_dev="/dev/mapper/cryptroot"
    local root_mount_pt="/mnt"

    # Install base system.
    partition_drive $DRIVE
    encrypt_drive $root_dev $DRIVE_PASSPHRASE
    format_filesystems $DRIVE
    mount_filesystems $boot_dev $swap_dev $crypt_dev $SNAPS_DIR
    install_base $root_mount_pt
    copy_files $root_mount_pt

    # Set up the system.
    chroot $root_mount_pt $DRIVE $DRIVE_PASSPHRASE $HOSTNAME $USER_NAME $USER_PASSPHRASE

    # Remove live medium and reboot.
    if [ $DRYRUN -eq 0 ]
    then
        local exitcode=$?
        if [ ! -f "$root_mount_pt/install/$THIS_SCRIPT" ] || [ $exitcode -ne 0 ]
        then
            echo "Exit Code: $exitcode"
            echo 'ERROR: Something failed inside the chroot'
            echo 'Not unmounting filesystems so you can investigate.'
            echo 'Make sure to unmount everything before trying to run this script again.'
        else
            echo 'Done! Poweroff, remove the live medium and reboot system.'
        fi
    fi
}

compute_swap_size()
{
    local swapsize=''

    local memsize=$(free -b | sed -n 's/^Mem:[\ \t]*\([0-9]*\)[\ \t]*.*/\1/p')

    if [ "$memsize" -lt 512000000 ]
    then
        swapsize='256MB'
    elif [ "$memsize" -lt 1000000000 ]
    then
        swapsize='512MB'
    elif [ "$memsize" -lt 2000000000 ]
    then
        swapsize='1GB'
    elif [ "$memsize" -lt 3000000000 ]
    then
        swapsize='1GB'
    elif [ "$memsize" -lt 4000000000 ]
    then
        swapsize='2GB'
    elif [ "$memsize" -lt 6000000000 ]
    then
        swapsize='2GB'
    elif [ "$memsize" -lt 8000000000 ]
    then
        swapsize='2GB'
    elif [ "$memsize" -lt 12000000000 ]
    then
        swapsize='3GB'
    elif [ "$memsize" -lt 16000000000 ]
    then
        swapsize='3GB'
    elif [ "$memsize" -lt 24000000000 ]
    then
        swapsize='4GB'
    elif [ "$memsize" -lt 32000000000 ]
    then
        swapsize='5GB'
    elif [ "$memsize" -lt 64000000000 ]
    then
        swapsize='6GB'
    elif [ "$memsize" -lt 128000000000 ]
    then
        swapsize='8GB'
    else
        swapsize='11GB'
    fi

    echo $swapsize
}

partition_drive()
{
    heading "Partitioning Drive"

    local dev=$1
    local swapsize=$(compute_swap_size)

    cmd "sgdisk -Z -o $dev"
    cmd "sgdisk -n 1:0:+300MB -t 1:ef00 $dev"
    cmd "sgdisk -n 2:0:+$swapsize -t 2:8200 $dev"
    cmd "sgdisk -n 3:0:0 -t 3:8300 $dev"
    cmd "sgdisk -p $dev"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

encrypt_drive()
{
    heading "Encrypting Root Partition"

    local dev=$1
    local passphrase=$2

    cmd "echo -n $passphrase" "|" "cryptsetup luksFormat -q $dev"
    cmd "echo -n $passphrase" "|" "cryptsetup luksOpen $dev cryptroot"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

format_filesystems()
{
    heading "Formatting Filesystem"

    local dev="$1"
    local boot_dev="$dev"1
    local swap_dev="$dev"2
    local crypt_dev="/dev/mapper/cryptroot"

    cmd "mkfs.fat -F 32 $boot_dev"
    cmd "mkswap $swap_dev"
    cmd "mkfs.btrfs $crypt_dev"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

mount_filesystems()
{
    heading "Mounting Filesystem"

    local boot_dev=$1
    local swap_dev=$2
    local crypt_dev=$3
    local snaps_dir=$4

    local root_mount_pt="/mnt"
    local root_subvol="@"
    local home_subvol="@home"
    local snaps_subvol="@snapshots"
    local var_subvol="@var"

    # Create BTRFS sub-volumes.
    cmd "mount $crypt_dev $root_mount_pt"
    cmd "pushd $root_mount_pt" ">" "/dev/null"
    cmd "btrfs subvolume create $root_mount_pt/$root_subvol"
    cmd "btrfs subvolume create $root_mount_pt/$home_subvol"
    cmd "btrfs subvolume create $root_mount_pt/$snaps_subvol"
    cmd "btrfs subvolume create $root_mount_pt/$var_subvol"
    cmd "popd" ">" "/dev/null"
    cmd "umount $root_mount_pt"

    local options="noatime,compress=zstd,space_cache=v2,discard=async,subvol="
    local home_mount_pt="$root_mount_pt/home"
    local snaps_mount_pt="$root_mount_pt$snaps_dir"
    local var_mount_pt="$root_mount_pt/var"
    local boot_mount_pt="$root_mount_pt/boot"

    # Mount filesystem.
    cmd "mount -o $options$root_subvol $crypt_dev $root_mount_pt"
    cmd "mkdir /mnt/home"
    cmd "mount -o $options$home_subvol /dev/mapper/cryptroot $home_mount_pt"
    cmd "mkdir /mnt/.snapshots"
    cmd "mount -o $options$snaps_subvol /dev/mapper/cryptroot $snaps_mount_pt"
    cmd "mkdir /mnt/var"
    cmd "mount -o $options$var_subvol /dev/mapper/cryptroot $var_mount_pt"
    cmd "mkdir $boot_mount_pt"
    cmd "mount $boot_dev $boot_mount_pt"
    cmd "swapon $swap_dev"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_base()
{
    heading "Installing Base System"

    local root_mount_pt=$1

    cmd "pacman -Syy --noconfirm"
    cmd "pacstrap $root_mount_pt base base-devel linux linux-headers linux-firmware"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

copy_files()
{
    heading "Copying files to chroot"

    local root_mount_pt=$1

    local fstab="/etc/fstab"

    # Create fstab.
    cmd "genfstab -U $root_mount_pt" ">>" "$root_mount_pt$fstab"

    cmd "mkdir $root_mount_pt/install"
    cmd "cp -r .wallpaper $root_mount_pt/install/"
    cmd "cp .bashrc $root_mount_pt/install/"
    cmd "cp .bash_profile $root_mount_pt/install/"
    cmd "cp .bash_prompt $root_mount_pt/install/"
    cmd "cp .bash_logout $root_mount_pt/install/"
    cmd "cp .Xauthority $root_mount_pt/install/"
    cmd "cp .Xdefaults $root_mount_pt/install/"
    cmd "cp .Xresources $root_mount_pt/install/"
    cmd "cp .xinitrc $root_mount_pt/install/"
    cmd "cp .xsettingsd $root_mount_pt/install/"
    cmd "cp .xprofile $root_mount_pt/install/"
    cmd "cp -r .config $root_mount_pt/install/"
    cmd "cp -r .xmonad $root_mount_pt/install/"
    cmd "cp .xmobarrc $root_mount_pt/install/"

    # Copy installation script onto installed system.
    cmd "cp $THIS_SCRIPT $root_mount_pt/install/"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

chroot()
{
    heading "Chrooting into installed system"

    local root_mount_pt=$1
    local drive=$2
    local drive_pass=$3
    local host=$4
    local user=$5
    local user_pass=$6

    # Run script inside installed system.
    if [ $DEBUG -eq 1 ]
    then
        cmd "arch-chroot $root_mount_pt bash install/$THIS_SCRIPT --chroot --drive $drive --drive-pass $drive_pass --host $host --user $user --user-pass $user_pass --debug --letr-rip"
    else
        cmd "arch-chroot $root_mount_pt bash install/$THIS_SCRIPT --chroot --drive $drive --drive-pass $drive_pass --host $host --user $user --user-pass $user_pass --letr-rip"
    fi

    heading "End of chroot"

    # Print phase 2 commands.
    if [ $DRYRUN -eq 1 ]; then phase_2; fi
}

phase_2()
{
    if [ $DEBUG -eq 1 ]
    then
        heading "Entered chroot. Configuring system."
        confirm
    fi

    local root_dev="$DRIVE"3
    local swap_dev="$DRIVE"2

    set_timezone $TIMEZONE
    set_locale
    set_hostname $HOSTNAME
    set_hosts $HOSTNAME
    set_users $USER_NAME $USER_PASSPHRASE

    install_bootloader $BOOTLOADER_TARGET
    install_microcode $MICROCODE
    add_btrfs_and_encryption_to_initial_ramdisk
    add_uuid_encrypted_device_to_grub $root_dev
    encrypt_swap_partition $swap_dev
    setup_boot_backups

    install_qemu_hypervisor $USER_NAME
    install_essential_packages

    install_x_windowing_system $USER_NAME $VIDEO_DRIVERS
    install_xmonad_tiling_window_manager $USER_NAME
    install_xmobar_panel $USER_NAME
    setup_bash_shell $USER_NAME
    setup_xterm_terminal_emulator $USER_NAME
    install_compositor
    install_conky
}

set_timezone()
{
    heading "Setting Timezone"

    local timezone=$1

    cmd "ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime"
    cmd "timedatectl set-local-rtc 0"
    cmd "hwclock --systohc"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

set_locale()
{
    heading "Setting Locale"

    cmd "echo LANG=en_US.UTF-8" ">" "/etc/locale.conf"
    cmd "sed -i s/#en_US.UTF-8/en_US.UTF-8/ /etc/locale.gen"
    cmd "locale-gen"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

set_hostname()
{
    heading "Setting Hostname"

    local hostname=$1

    cmd "echo $hostname" ">" "/etc/hostname"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

set_hosts()
{
    heading "Setting Hosts"

    local hostname=$1
    local hostsfile="/etc/hosts"

    cmd "echo -e 127.0.0.1 localhost" ">>" "$hostsfile"
    cmd "echo -e ::1 localhost" ">>" "$hostsfile"
    cmd "echo -e 127.0.1.1 $hostname.localdomain $hostname" ">>" "$hostsfile"

    cmd "pacman -S --noconfirm networkmanager network-manager-applet"
    cmd "systemctl enable NetworkManager"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

set_users()
{
    heading "Setting Users"

    local user=$1
    local passwd=$2

    cmd "useradd -m -s /bin/bash -G wheel $user"
    cmd "chpasswd" "<<<" "$user:$passwd"
    EDITOR="sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/'"
    cmd "visudo"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_bootloader()
{
    heading "Installing Bootloader"

    local target=$1
    local boot_dir="/boot"
    local grub_id="GRUB"

    cmd "pacman -S --noconfirm grub efibootmgr"
    cmd "grub-install --target=$target --efi-directory=$boot_dir --bootloader-id=$grub_id"
    cmd "grub-mkconfig -o $boot_dir/grub/grub.cfg"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_microcode()
{
    heading "Installing Microcode"

    local microcode=$1
    local boot_dir="/boot"

    cmd "pacman -S --noconfirm $microcode"
    cmd "grub-mkconfig -o $boot_dir/grub/grub.cfg"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

add_btrfs_and_encryption_to_initial_ramdisk()
{
    heading "Adding BTRFS and Encryption to Initial Ramdisk"

    local file="/etc/mkinitcpio.conf"

    cmd "sed -i s/^MODULES=()$/MODULES=(btrfs)/ $file"

    local hooks_beg="HOOKS=(base udev autodetect modconf block"
    local hooks_end="filesystems keyboard fsck)"
    # The fsck causes the mknitcpio command to exit with non-zero code.
    # This script stops if a command exits with non-zero.
    # So, remove the fsck.
    local hooks_end_no_fsck="filesystems keyboard)"

    sedcmd "^$hooks_beg $hooks_end" "$hooks_beg encrypt $hooks_end_no_fsck" "$file"
    cmd "mkinitcpio -p linux"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

add_uuid_encrypted_device_to_grub()
{
    heading "Adding Encrypted Device to GRUB"

    local dev="$1"
    local boot_dir="/boot"

    local uuid=$(blkid | grep "$dev" | sed -n 's|.*UUID="\([A-Za-z0-9-]*\)" TYPE=.*|\1|p')
    local default="loglevel=3 quiet cryptdevice=UUID=$uuid:cryptroot root=/dev/mapper/cryptroot"
    local default_head="GRUB_CMDLINE_LINUX_DEFAULT"

    sedcmd "$default_head.*" "$default_head=\"$default\"" "/etc/default/grub"
    cmd "grub-mkconfig -o $boot_dir/grub/grub.cfg"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

encrypt_swap_partition()
{
    heading "Encrypting swap partition"

    local swap_dev=$1

    # Create small filesystem in swap partition to hold the LABEL.
    # This small filesystem will not be encrypted, otherwise the LABEL will get erased.
    # https://wiki.archlinux.org/title/Dm-crypt/Swap_encryption
    local label="cryptswap"
    local size="1M"
    cmd "sudo swapoff $swap_dev"
    cmd "mkfs.ext2 -F -L $label $swap_dev $size"

    # Add to crypt table.
    local offset="2048"
    local passwd="/dev/urandom"
    local size="512"
    local file="/etc/crypttab"
    local find="^# swap.*"
    local replace="swap LABEL=$label $passwd swap,offset=$offset,cipher=aes-xts-plain64,size=$size"
    sedcmd "$find" "$replace" "$file"

    # Replace UUID of swap device with /dev/mapper/swap in /etc/fstab.
    lineno=$(sed -n "\|$swap_dev|=" /etc/fstab)
    lineno=$((lineno+1))
    sed_n_cmd "$lineno" "UUID=[0-9A-Za-z-]*" "/dev/mapper/swap" "/etc/fstab"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_qemu_hypervisor()
{
    heading "Installing Virt-Manager QEMU/KVM Hypervisor"

    local user=$1

    cmd "sudo pacman -S --noconfirm virt-manager qemu qemu-arch-extra edk2-ovmf dnsmasq dmidecode"
    cmd "sudo systemctl enable libvirtd.service"
    cmd "sudo systemctl enable virtlogd.service"
    cmd "sudo usermod -G libvirt -a $user"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

setup_boot_backups()
{
    heading "Setting up boot partition backups"

    cmd "pacman -S --noconfirm rsync"

    local file='/usr/share/libalpm/hooks/50_bootbackup.hook'

    cmd "echo [Trigger]"                                           ">>" "$file"
    cmd "echo Operation = Upgrade"                                 ">>" "$file"
    cmd "echo Operation = Install"                                 ">>" "$file"
    cmd "echo Operation = Remove"                                  ">>" "$file"
    cmd "echo Type = Path"                                         ">>" "$file"
    cmd "echo Target = usr/lib/modules/*/vmlinuz"                  ">>" "$file"
    cmd "echo"                                                     ">>" "$file"
    cmd "echo [Action]"                                            ">>" "$file"
    cmd "echo Depends = rsync"                                     ">>" "$file"
    cmd "echo Description = Backing up /boot..."                   ">>" "$file"
    cmd "echo When = PostTransaction"                              ">>" "$file"
    cmd "echo Exec = /usr/bin/rsyn -a --delete /boot /.bootbackup" ">>" "$file"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_essential_packages()
{
    heading "Installing essential packages"

    cmd "sudo pacman -S --noconfirm nano vim emacs man-db reflector udisks2"
    cmd "sudo pacman -S --noconfirm firefox chromium gimp inkscape"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_x_windowing_system()
{
    heading "Installing windowing system"

    local args=( "$@" )
    local user=$1
    local video_drivers=${args[@]:1}

    cmd "pacman -S --noconfirm $video_drivers"
    cmd "pacman -S --noconfirm xorg-server xorg-xrdb xorg-xinit lxsession"
    cmd "pacman -S --noconfirm xorg-server xwallpaper xdotool"
    cmd "pacman -S --noconfirm ttf-dejavu ttf-liberation"

    cmd "cp /install/.Xauthority /home/$user/"
    cmd "cp /install/.Xdefaults /home/$user/"
    cmd "cp /install/.Xresources /home/$user/"
    cmd "cp /install/.xinitrc /home/$user/"
    cmd "cp /install/.xsettingsd /home/$user/"
    cmd "cp /install/.xprofile /home/$user/"
    cmd "chown $user:$user /home/$user/.Xauthority"
    cmd "chown $user:$user /home/$user/.Xdefaults"
    cmd "chown $user:$user /home/$user/.Xresources"
    cmd "chown $user:$user /home/$user/.xinitrc"
    cmd "chown $user:$user /home/$user/.xsettingsd"
    cmd "chown $user:$user /home/$user/.xprofile"
    #cmd "xrdb /home/$user/.Xresources"

    # Wallpaper.
    cmd "cp -r /install/.wallpaper /home/$user/"
    cmd "chown -R $user:$user /home/$user/.wallpaper"

    # File explorer.
    cmd "pacman -S --noconfirm ranger w3m"

    # Applications configuration.
    cmd "cp -r /install/.config /home/$user/"
    cmd "chown -R $user:$user /home/$user/.config"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_xmonad_tiling_window_manager()
{
    heading "Installing Xmonad Tiling Window Manager"

    local user=$1

    cmd "pacman -S --noconfirm xmonad xmonad-contrib dmenu"
    cmd "pacman -S --noconfirm trayer qalculate-gtk volumeicon"

    # Configuration.
    cmd "cp -r /install/.xmonad /home/$user/"
    cmd "chown -R $user:$user /home/$user/.xmonad"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_xmobar_panel()
{
    heading "Installing Xmonad Tiling Window Manager"

    local user=$1

    cmd "pacman -S --noconfirm xmobar ttf-font-awesome"

    # Configuration.
    cmd "cp /install/.xmobarrc /home/$user/"
    cmd "chown $user:$user /home/$user/.xmobarrc"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

setup_bash_shell()
{
    heading "Setting up Bash shell"

    local user=$1

    # Shell prompt.
    #install_starship_shell_prompt

    # System information.
    cmd "pacman -S --noconfirm neofetch"

    cmd "cp /install/.bashrc /home/$user/"
    cmd "cp /install/.bash_profile /home/$user/"
    cmd "cp /install/.bash_prompt /home/$user/"
    cmd "cp /install/.bash_logout /home/$user/"

    cmd "chown $user:$user /home/$user/.bashrc"
    cmd "chown $user:$user /home/$user/.bash_profile"
    cmd "chown $user:$user /home/$user/.bash_prompt"
    cmd "chown $user:$user /home/$user/.bash_logout"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

setup_xterm_terminal_emulator()
{
    heading "Setting up the terminal emulator"

    local user=$1

    cmd "pacman -S --noconfirm xterm"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_compositor()
{
    heading "Installing Picom"

    cmd "pacman -S --noconfirm picom"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_conky()
{
    heading "Installing Conky"

    cmd "pacman -S --noconfirm conky"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

install_firewall()
{
    heading "Installing firewall"

    cmd "pacman -S --noconfirm firewalld"
    cmd "systemctl enable --now firewalld"
    cmd "firewall-cmd --set-default-zone=home"
    cmd "firewall-cmd --add-service libvirt --zone=libvirt --permanent"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

setup_btrfs_snapshots()
{
    # We can take snapshots of the system (the root subvolume) to recover from
    # bad installations or bad system updates.
    # Snapshots will be taken only from the root @ subvolume.
    # Other subvolumes are not included, even if they are under the root @.
    # For data, we use a different strategy: backups.

    heading "Setting up BTRFS root snapshots"

    local user=$1
    local snaps_dir=$2

    cmd "pacman -S --noconfirm btrfs-progs snapper"

    cmd "umount $snaps_dir"
    cmd "rm -r $snaps_dir"

    # Create snapshot configuration named "root".
    # This creates a snapshot subvolume, so must delete it beacuse we already have one.
    local snap_conf="root"
    local root_dir="/"
    cmd "snapper -c $snap_conf create-config $root_dir"
    cmd "btrfs subvolume delete $snaps_dir"

    # Re-create the snapshots directory that we deleted at the beginning.
    cmd "mkdir $snaps_dir"
    cmd "mount -a"
    cmd "chmod 750 $snaps_dir"

    # Allow the user to to have access and set up timeline and cleanup.
    local file="/etc/snapper/configs/$snap_conf"
    sedcmd '^ALLOW_USERS=""' "ALLOW_USERS=\"$user\"" "$file"
    sedcmd 'TIMELINE_LIMIT_HOURLY="[0-9]*"' 'TIMELINE_LIMIT_HOURLY="5"' "$file"
    sedcmd 'TIMELINE_LIMIT_DAILY="[0-9]*"' 'TIMELINE_LIMIT_DAILY="7"' "$file"
    sedcmd 'TIMELINE_LIMIT_WEEKLY="[0-9]*"' 'TIMELINE_LIMIT_WEEKLY="0"' "$file"
    sedcmd 'TIMELINE_LIMIT_MONTHLY="[0-9]*"' 'TIMELINE_LIMIT_MONTHLY="0"' "$file"
    sedcmd 'TIMELINE_LIMIT_YEARLY="[0-9]*"' 'TIMELINE_LIMIT_YEARLY="0"' "$file"


    # Enable snapshots.
    cmd "systemctl enable snapper-timeline.timer"
    cmd "systemctl enable snapper-cleanup.timer"

    # Take snapshot of initial system.
    #cmd "snapper -c $snap_conf create --description initial-system"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

unmount_filesystems()
{
    heading "Unmounting filesystems"

    local swap_dev="$DRIVE"2

    cmd "umount -a"
    cmd "swapoff $swap_dev"
    cmd "cryptsetup luksClose /dev/mapper/cryptroot"

    heading "OK"

    if [ $DEBUG -eq 1 ]; then confirm; fi
}

get_uuid()
{
    blkid -o export "$1" | grep UUID | awk -F= '{print $2}'
}

hash_passphrase()
{
    local passphrase=$1
    openssl passwd -1 $passphrase
}

usage()
  {
      cat <<EOF

Usage: bash $0 [-h] [--help] [--debug] [--letr-rip]
               [--chroot] [--drive drive] [--drive-pass pass]
               [--host host] [--user user] [--user-pass pass]

This script installs Arch Linux with select packages.
Most of the options are for automation.
A human will only use:
  -h|--help
  --debug
  --letr-rip

-h|--help               print this help menu
   --chroot             whether running in chroot
   --drive      string  drive where to install the system
   --drive-pass string  drive encryption password
   --host       string  host name
   --user       string  user name
   --user-pass  string  user password
   --debug              ask for confirmation before executing each command
   --letr-rip           not just print the commands, execute them

examples:
  dry-run:
    bash $0
    bash $0 --debug
  install system:
    bash $0 --letr-rip

EOF
  }

confirm()
{
    read -p "Continue? [Y/n]: " confirm && [[ $confirm == "" || $confirm == [yY] ]] || exit
}

line()
{
    # `printf '\055%.0s' {1..80}` creates 80 dashes:
    # \055 is ascii for dash
    # CHAR%.0s will always print CHAR no matter what argument is given
    # {1..80} expands to 1 2 3 ... 80
    printf '\055%.0s' {1..80}
}

heading()
{
    local line=`line`
    local message=$1

    # ${line:${#1}} trims line by number of characters in $1.
    printf "%s $message\n" "${line:${#1}}"
}

cmd()
{
    # Determine whether compund command or simple command.
    if [[ $# -eq 3 ]]
    then
        local compound=2
    elif [[ $# -eq 1 ]]
    then
        local compound=1
    else
        echo "Bad command $@"
        exit 1
    fi

    # Print command.
    if [ $DRYRUN = 1 ]; then
        if [[ $compound -eq 2 ]]; then echo $1 $2 $3; fi
        if [[ $compound -eq 1 ]]; then echo $1; fi

    # Execute command. Abort script if command fails.
    else
        # Capture stderr while letting stdout through.
        # https://stackoverflow.com/questions/13806626/capture-both-stdout-and-stderr-in-bash
        #{ ERROR="$($1 2>&1 1>&3-)" ;} 3>&1

        if [[ $compound -eq 2 ]];
        then
            if [ $2 == "|" ];
            then
                $1 | $3
                local exitcode=$?
            elif [ $2 == ">" ];
            then
                $1 > $3
                local exitcode=$?
            elif [ $2 == ">>" ];
            then
                $1 >> $3
                local exitcode=$?
            elif [ $2 == "<<<" ];
            then
                $1 <<< $3
                local exitcode=$?
            elif [ $1 == "sed -i" ]
            then
                $1 '$2' $3
                local exitcode=$?
            else
                echo "Bad command $@"
                exit 1
            fi
        fi
        if [[ $compound -eq 1 ]]
        then
            $1
            local exitcode=$?
        fi

        # Process error.
        # Relies on 'set -o pipefail' to catch errors in pipelines.
        #if [ $DEBUG -eq 1 ] && [ $exitcode -ne 0 ]; then
        if [ $exitcode -ne 0 ]; then
            if [[ $compound -eq 2 ]]; then echo "Command: " $1 $2 $3; fi
            if [[ $compound -eq 1 ]]; then echo "Command: " $1; fi
            exit $exitcode
        fi
    fi
}

sedcmd()
{
    local find=$1
    local replace=$2
    local file=$3

    local cmdstr="sed -i 's|$find|$replace|' $file"

    # Print command.
    if [ $DRYRUN = 1 ]; then
        echo $cmdstr

    # Execute command. Abort script if command fails.
    else
        # Execute sed command.
        sed -i "s|$find|$replace|" $file
        local exitcode=$?

        # Process error.
        #if [ $DEBUG -eq 1 ] && [ $exitcode -ne 0 ]; then
        if [ $exitcode -ne 0 ]; then
            echo "Command: $cmdstr"
            exit $exitcode
        fi
    fi
}

sed_n_cmd()
{
    local lineno=$1
    local find=$2
    local replace=$3
    local file=$4

    local cmdstr="sed -i '$lineno s|$find|$replace|' $file"

    # Print command.
    if [ $DRYRUN = 1 ]; then
        echo $cmdstr

    # Execute command. Abort script if command fails.
    else
        # Execute sed command.
        sed -i "$lineno s|$find|$replace|" $file
        local exitcode=$?

        # Process error.
        #if [ $DEBUG -eq 1 ] && [ $exitcode -ne 0 ]; then
        if [ $exitcode -ne 0 ]; then
            echo "Command: $cmdstr"
            exit $exitcode
        fi
    fi
}

list_devices()
{
    local devices=("$@")

    for idx in "${!devices[@]}"
    do
        ret[idx]="$idx:${devices[$idx]}"
    done

    echo "${ret[*]}"
}

run_phase_1()
{
    local name
    local devices
    local default
    local passphrase
    local confirmation

    devices=($(lsblk | grep '^[sv]d[a-z].*disk' | cut -d ' ' -f1 | sed -n 's|\(.*\)|/dev/\1|p'))
    if [ ! $DRIVE ]
    then
        read -p "Drive [$(list_devices ${devices[@]})] (default 0): " N
        if [ ! $N ]; then N=0; fi
        if [ "$N" -ge "${#devices[*]}" ]; then echo "Invalid selection"; exit; fi
        DRIVE=${devices[$N]}
    fi

    # Cannot put this in a function because of the empty echos.
    default="secret"
    if [ ! $DRIVE_PASSPHRASE ]
    then
        read -s -p "Drive Passphrase (default $default): " passphrase
        echo
        if [ ! $passphrase ]
        then
            DRIVE_PASSPHRASE=$default
        else
            read -s -p "Confirm: " confirmation
            echo
            if [ $passphrase != $confirmation ]
            then
                echo "Passphrase mismatch"
                exit
            fi
            DRIVE_PASSPHRASE=$passphrase
        fi
    fi

    # Cannot put this in a separate function because of the empty echos.
    default="arch"
    if [ ! $HOSTNAME ]
    then
        read -p "Host (default $default): " name
        if [ ! $name ]; then name=$default; fi
        HOSTNAME=$name
    fi

    # Cannot put this in a separate function because of the empty echos.
    default="user"
    if [ ! $USER_NAME ]
    then
        read -p "User (default $default): " name
        if [ ! $name ]; then name=$default; fi
        USER_NAME=$name
    fi

    # User passphrase.
    default="secret"
    if [ ! $USER_PASSPHRASE ]
    then
        read -s -p "User Passphrase (default $default): " passphrase
        echo
        if [ ! $passphrase ]
        then
            USER_PASSPHRASE=$default
        else
            read -s -p "Confirm: " confirmation
            echo
            if [ $passphrase != $confirmation ]
            then
                echo "Passphrase mismatch"
                exit
            fi
            USER_PASSPHRASE=$passphrase
        fi
    fi

    # Confirmation before installing.
    echo "Drive: $DRIVE, Host: $HOSTNAME, User: $USER_NAME"
    confirm

    # Set up base system.
    phase_1
}

run_phase_2()
{
    phase_2
}

#set -e  # exit if command fails
#set -v  # print script
#set -x  # print command before executing
set -u  # error if a variable is referenced before being set
set -o pipefail  # fail pipeline command if any sub-command fails

echo "*****************************"
echo "** Arch Linux Installation **"
echo "*****************************"

THIS_SCRIPT=$(basename "$0")
DRYRUN=1
DEBUG=0
CHROOT=0

while [[ $# -gt 0 ]]
do
    case "$1" in
        -h|--help)
            usage
            exit
            ;;
        --chroot)
            CHROOT=1
            shift
            ;;
        --drive)
            shift
            DRIVE=$1
            shift
            ;;
        --drive-pass)
            shift
            DRIVE_PASSPHRASE=$1
            shift
            ;;
        --host)
            shift
            HOSTNAME=$1
            shift
            ;;
        --user)
            shift
            USER_NAME=$1
            shift
            ;;
        --user-pass)
            shift
            USER_PASSPHRASE=$1
            shift
            ;;
        --debug)
            DEBUG=1
            shift
            ;;
        --letr-rip)
            DRYRUN=0
            shift
            ;;
        *)
            echo "Unknown option: '$1'"
            usage
            exit
            ;;
    esac
done

# Phase 1 of the installation: Base system install.
if [ $CHROOT -eq 0 ]
then
    run_phase_1
# Phase 2 of the installation: System configuration and additional packages.
else
    run_phase_2
fi

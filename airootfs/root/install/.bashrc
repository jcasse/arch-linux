#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias l='ls -al'
alias emacs='emacs -nw'
PS1='[\u@\h \W]\$ '
source .bash_prompt

# Show system information
neofetch

# Ranger file manager
# Prevent nested ranger instances
# https://wiki.archlinux.org/title/Ranger
ranger()
{
    if [ -z "$RANGER_LEVEL" ]
    then
	/usr/bin/ranger "$@"
    else
	exit
    fi
}

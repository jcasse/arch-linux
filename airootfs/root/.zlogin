# fix for screen readers
if grep -Fq 'accessibility=' /proc/cmdline &> /dev/null; then
    setopt SINGLE_LINE_ZLE
fi

~/.automated_script.sh

pushd install > /dev/null
bash arch-install.sh --letr-rip
popd > /dev/null
